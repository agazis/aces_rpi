import time
import usbiss
import opc
import sqlite3
import os.path
import csv
#import signal

###########
# Params
###########

# Sensor params
sampling_period = 10 # period at which we are sampling the sensor, in seconds
sensor_connect_retry_time = 5 # time to wait between connection retries
sensor_init_time = 10         # initialisation time in seconds

# DB params
dbname = 'mtof_tests.db'



# Guts
runflag = True			# Logging only proceeds when runflag is True




###########

def init_db(db_filename):

	try:
		conn = sqlite3.connect(db_filename)
	except Error as e:
		return None

	c = conn.cursor()
	# Create tables
	# first, the "opc" table which will hold the actual measurements
	c.execute('''CREATE TABLE IF NOT EXISTS opc
	             (id INTEGER PRIMARY KEY AUTOINCREMENT, session_id INTEGER,
	             bin0 REAL, bin1 REAL, bin2 REAL, bin3 REAL, bin4 REAL, bin5 REAL, bin6 REAL, bin7 REAL, bin8 REAL, bin9 REAL, bin10 REAL, bin11 REAL, bin12 REAL, bin13 REAL, bin14 REAL, bin15 REAL, 
	             bin1_mtof REAL, bin3_mtof REAL, bin5_mtof REAL, bin7_mtof REAL,
	             sfr REAL, sampling_period REAL,
	             pm1 REAL, pm2_5 REAL, pm10 REAL,
	             checksum REAL, temperature REAL, pressure REAL, timestamp TEXT, unix_time REAL
	             )''')

	# Check latest session ID, we will use it as a label for all logging events while this script is running
	c.execute('''SELECT session_id FROM opc ORDER BY id DESC LIMIT 1''')
	rows = c.fetchall()
	if not rows:
		session_id = 1
	else:
		session_id = rows[0][0] + 1
	conn.commit()
	return (conn, session_id)


# Fire up the db connection
(conn, session_id) = init_db(dbname)
c = conn.cursor()

# Connect to the sensor 
print("Connecting to sensor")
spi = usbiss.USBISS("/dev/ttyACM0", 'spi', spi_mode = 2, freq = 500000) # Open an SPI connection
alpha = None
while alpha is None:
  try:
    alpha = opc.OPCN2(spi)
  except opc.exceptions.FirmwareVersionError:
    print("Sensor is having a bad day, will try again in " + str(sensor_connect_retry_time) + " seconds")
    time.sleep(sensor_connect_retry_time)

# If we reach here, we have successfully connected so turn OPC on
alpha.on()
print("Sensor connected, initialising...")
for i in range(sensor_init_time):
  print(sensor_init_time - i)
  time.sleep(1)
alpha.toggle_fan(False)
time.sleep(2)
print("READY")
print("Connected to DB " + '"' + dbname + '"' + "\nSession ID: " + str(session_id))



################################
# Prep CSV file fields
# alphasense keys + Timestamp
################################
fieldnames = ['Bin 0', 'Bin 1', 'Bin 2', 'Bin 3', 'Bin 4', 'Bin 5', 'Bin 6',
              'Bin 7', 'Bin 8', 'Bin 9', 'Bin 10', 'Bin 11', 'Bin 12',
              'Bin 13', 'Bin 14', 'Bin 15', 'Bin1 MToF', 'Bin3 MToF',
              'Bin5 MToF', 'Bin7 MToF', 'SFR', 'Sampling Period',
              'PM1', 'PM2.5', 'PM10', 'Checksum', 'Temperature', 'Pressure',
              'Timestamp', 'UNIX Time']


while True:

	if runflag:

		data = alpha.histogram(number_concentration=True) # set to False for counts
		data['Timestamp'] = time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime()) # Add timestamp
		# Add unix time
		data['UNIX Time'] = time.time()

		print(str(data['Timestamp']) + " - " + str(data['UNIX Time']))

		###################
		# Add data to CSV #
		###################
		# Check if data file exists for this day
		# If true, append data to file
		# Else, make new file and write data to it (incl header/fieldnames)
		time_str=time.strftime("%y%m%d", time.gmtime())
		if os.path.isfile('mtof_test_'+time_str+'.csv'):
		    with open('mtof_test_'+time_str+'.csv','a') as f:
		        w = csv.DictWriter(f, fieldnames, dialect='excel')
		        w.writerow(data)
		else:
		    with open('mtof_test_'+time_str+'.csv','wb') as f:
		        w = csv.DictWriter(f, fieldnames, dialect='excel')
		        w.writeheader()
		        w.writerow(data)

		###################
		# Add data to DB  #
		###################
		c.execute('''INSERT INTO opc (session_id, bin0, bin1, bin2, bin3, bin4, bin5, bin6, bin7, bin8, bin9, bin10, bin11, bin12, bin13, bin14, bin15, 
		         bin1_mtof, bin3_mtof, bin5_mtof, bin7_mtof,
		         sfr, sampling_period,
		         pM1, pm2_5, pm10,
		         checksum, temperature, pressure, timestamp, unix_Time)
		         VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 
		         ?, ?, ?, ?, 
		         ?, ?, 
		         ?, ?, ?, 
		         ?, ?, ?, ?,
		         ?)''',
		         [session_id, data['Bin 0'], data['Bin 1'], data['Bin 2'], data['Bin 3'], data['Bin 4'], data['Bin 5'], data['Bin 6'], data['Bin 7'], data['Bin 8'], data['Bin 9'], data['Bin 10'], data['Bin 11'], data['Bin 12'], data['Bin 13'], data['Bin 14'], data['Bin 15'], 
		         data['Bin1 MToF'], data['Bin3 MToF'], data['Bin5 MToF'], data['Bin7 MToF'],
		         data['SFR'], data['Sampling Period'], 
		         data['PM1'], data['PM2.5'], data['PM10'],
		         data['Checksum'], data['Temperature'], data['Pressure'], data['Timestamp'],
		         data['UNIX Time']
		         ])
		conn.commit()

	time.sleep(sampling_period) # pause before reading data again
