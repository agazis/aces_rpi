var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');



var index = require('./routes/index');
//var users = require('./routes/users');
var api = require('./routes/api');

var app = express();



// #########################################
// SOCKET.IO
// #########################################

// This will fetch PID of the sensor handling python script (systemd service OPC_sensor)
//const { execSync } = require('child_process');
//sensor_service_pid = parseInt((((execSync('systemctl show -p MainPID OPC_sensor.service')).toString().trim())).split('=')[1]);


var db = require('sqlite');
db.open('/home/pi/SKARL/Dronekit_code/particles.db');


async function getLastDBRecord() {
	let lr;
	try {
		lr = await db.all('SELECT * FROM opc ORDER BY id DESC LIMIT 1');
	} catch (err) {
		console.log('Some bullshit happened trying to fetch record from db:');
		console.log(err);
	}
	return lr;
}

async function getHWstatus() {
	let lr;
	try {
		lr = await db.all('SELECT * FROM hwstatus');
	} catch (err) {
		console.log('Some bullshit happened trying to fetch hardware status from db:');
		console.log(err);
	}
	return lr;
}


var server = require('http').Server(app);
var io = require('socket.io')(server);

server.listen(4000);

app.get('/', function (req, res) {
  res.sendFile(__dirname + '/public/index.html');
});


io.on('connection', function (socket) {

	var last_id = null;
	var last_hwstatus = null;

	// Repeat this at set intervals
	setInterval(async function() {
		last_rec = await getLastDBRecord();
		if ((last_id != last_rec[0].id) && (last_id != null)) {
			socket.emit('incoming_data', last_rec[0]);
			last_id = last_rec[0].id
		}
	}, 1000);
	setInterval(async function() {
		hwstatus = (await getHWstatus())[0];
		hwstatus_string = JSON.stringify(hwstatus);
		if (last_hwstatus != hwstatus_string) {
			last_hwstatus = hwstatus_string;
			socket.emit('hwstatus', hwstatus);
		}

	}, 500);

	// Handle incoming
	socket.on('my other event', function (data) {
    console.log(data);
  });
});



// #########################################






// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', index);
//app.use('/users', users);
app.use('/api', api);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
