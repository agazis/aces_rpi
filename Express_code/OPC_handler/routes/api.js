var express = require('express');
var router = express.Router();
var db = require('sqlite');

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.get('/data', async function(req, res, next) {
  var since = parseInt(req.query.since || 0);
  var rows = await db.all('SELECT * FROM OPC WHERE id > ? ORDER BY id DESC LIMIT 150', since);
  rows.reverse();
  res.json(rows);
});

db.open('/home/pi/SKARL/Dronekit_code/particles.db');

module.exports = router;
