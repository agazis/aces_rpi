async function get_data(index) {

    string = '/api/data';
    if (typeof index === 'number') {
        string = string + '?since=' + index;
    }

    var response = await axios.get(string);
    var data = response.data;
    console.log(index);
    console.log('Request string: ' + string);
    console.log('you have downloaded ' + data.length + ' rows of data!');
    console.log(data);

    return data;

}