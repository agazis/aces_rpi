from sampling_vehicle import SamplingVehicle
from dronekit import connect, Vehicle
import time
import subprocess
import usbiss
import opc
import sqlite3
import os.path
import sys
import csv
import signal
import argparse


##################
# Parse arguments
##################
parser = argparse.ArgumentParser()
parser.add_argument("--static", help="Do not try to connect to GPS. Use in conjunction with --set-location to manually give location. When used, time is not set from GPS so ideally use when internet connectivity is available to avoid garbage timestamps.", action='store_true')
parser.add_argument("--set-location", help="Ignores GPS and manually sets location (decimal degrees, WGS84, altitude in metres), which will be constant throughout log. Use it in conjuntion with --static to ensure a location is provided to the log", nargs=3, metavar=('LAT', 'LON', 'ALT'))
args = parser.parse_args()

if not args.set_location == None:
	try:
		manual_latitude = float(args.set_location[0])
	except ValueError:
		print("Could parse not passed Latitude, exiting")
		sys.exit(2)
	try:
		manual_longitude = float(args.set_location[1])
	except ValueError:
		print("Could parse not passed Longitude, exiting")
		sys.exit(2)
	try:
		manual_altitude = float(args.set_location[2])
	except ValueError:
		print("Could parse not passed Altitude, exiting")
		sys.exit(2)
	manual_location_flag = True
else:
	manual_location_flag = False

use_autopilot_flag = not args.static



###########
# Params
###########
# Autopilot connection
autopilot_port = "/dev/ttyS0" 
pixhawk_baud_rate = 921600
connection_timeout = 15

# Sensor params
sampling_period = 2 # period at which we are sampling the sensor, in seconds
sensor_connect_retry_time = 5 # time to wait between connection retries
sensor_init_time = 10         # initialisation time in seconds

# DB params
dbname = 'particles.db'



# Guts
runflag = True			# Logging only proceeds when runflag is True


###########



def signal_handler(signal, frame):
	''' Captures SIGUSR1 and SIGUSR2 and starts/stops logging respectively.
	'''
	global runflag
	if signal == 10:		# 10 is SIGUSR1
		runflag = True
	elif signal == 12:		# 12 is SIGUSR2
		runflag = False

def init_db(db_filename):
	''' Creates a db connection to the sqlite db specified by passed filename and returns a connection object and session ID.
		If the file does not exist, it will be created.
		If the connection fails, None is returned instead.

		The db is assumed to contain tables "opc" and "hwstatus"
		The values of "hwstatus" are reset to default by this function:
		(autopilot = "Disconnected", gps = "No fix", clock = "No sync", sensor = "Offline")
		
		The last session_id entry in table "opc" (if it exists) is returned incremented by one to be used in the current session\
		The normal return value is a tuple of (connection object, session ID)
		'''

	try:
		conn = sqlite3.connect(db_filename)
	except Error as e:
		return None

	c = conn.cursor()
	# Create tables
	# first, the "opc" table which will hold the actual measurements
	c.execute('''CREATE TABLE IF NOT EXISTS opc
	             (id INTEGER PRIMARY KEY AUTOINCREMENT, session_id INTEGER,
	             bin0 REAL, bin1 REAL, bin2 REAL, bin3 REAL, bin4 REAL, bin5 REAL, bin6 REAL, bin7 REAL, bin8 REAL, bin9 REAL, bin10 REAL, bin11 REAL, bin12 REAL, bin13 REAL, bin14 REAL, bin15 REAL, 
	             bin1_mtof REAL, bin3_mtof REAL, bin5_mtof REAL, bin7_mtof REAL,
	             sfr REAL, sampling_period REAL,
	             pm1 REAL, pm2_5 REAL, pm10 REAL,
	             checksum REAL, temperature REAL, pressure REAL, timestamp TEXT,
	             latitude REAL, longitude REAL, altitude REAL, airspeed REAL, unix_time REAL
	             )''')
	# second, the "hwstatus" table, which will hold the hardware status values
	c.execute('''CREATE TABLE IF NOT EXISTS hwstatus (autopilot TEXT, gps TEXT,  clock TEXT, sensor TEXT)''')
	# Set all hardware status fields to default values
	c.execute('''SELECT * FROM hwstatus''')		# first check if hwstatus has actually been populated before, if not we need to initialise it by inserting one row (it will only ever have one)
	rows = c.fetchall()
	if not rows:
		c.execute('''INSERT INTO hwstatus (autopilot, gps, clock, sensor) VALUES ("Disconnected", "No fix", "No sync", "Offline")''')
	else:
		c.execute('''UPDATE hwstatus SET autopilot = "Disconnected", gps = "No fix", clock = "No sync", sensor = "Offline"''')	


	# Check latest session ID, we will use it as a label for all logging events while this script is running
	c.execute('''SELECT session_id FROM opc ORDER BY id DESC LIMIT 1''')
	rows = c.fetchall()
	if not rows:
		session_id = 1
	else:
		session_id = rows[0][0] + 1
	conn.commit()
	return (conn, session_id)




# Register the signal handler for SIGUSR1 and SIGUSR2 (start/stop logging)
signal.signal(signal.SIGUSR1, signal_handler)
signal.signal(signal.SIGUSR2, signal_handler)


# Fire up the db connection
(conn, session_id) = init_db(dbname)
c = conn.cursor()

# Connect to the autopilot
if use_autopilot_flag:
	vehicle = None
	while vehicle == None:
		try:
			vehicle = connect(autopilot_port, baud=pixhawk_baud_rate, wait_ready=True, vehicle_class=SamplingVehicle, heartbeat_timeout = connection_timeout)
		except Exception as e:
			print(e)
			print("Reconnecting")
			vehicle = None
	c.execute('''UPDATE hwstatus SET autopilot = "Connected"''')
	conn.commit()

	# Sync system clock to GPS time from autopilot
	print("Waiting for GPS time")
	while vehicle.unix_time_gps.time_unix_usec == 0:
	  time.sleep(1)
	print("GPS time received, syncing system clock to GPS timestamp:")
	c.execute('''UPDATE hwstatus SET gps = "Fix"''')
	conn.commit()

	try:
	  subprocess.check_call(["sudo", "date", "+%s", "-s", "@"+str(vehicle.unix_time_gps.time_unix_usec/1000000.0)])
	except CalledProcessError as cpe:
	  print("Error setting system clock, exiting")
	  print(cpe)
	  c.execute('''UPDATE hwstatus SET clock = "ERROR SETTING SYSTEM CLOCK"''')
	  conn.commit()
	  sys.exit(1)
	print("System clock synced succesfully to GPS time")
	c.execute('''UPDATE hwstatus SET clock = "Synced"''')
	conn.commit()


# Connect to the sensor 
print("Connecting to sensor")
spi = usbiss.USBISS("/dev/ttyACM0", 'spi', spi_mode = 2, freq = 500000) # Open an SPI connection
alpha = None
while alpha is None:
  try:
    alpha = opc.OPCN2(spi)
  except opc.exceptions.FirmwareVersionError:
    print("Sensor is having a bad day, will try again in " + str(sensor_connect_retry_time) + " seconds")
    c.execute('''UPDATE hwstatus SET sensor = "Retrying connection"''')
    conn.commit()
    time.sleep(sensor_connect_retry_time)

# If we reach here, we have successfully connected so turn OPC on
alpha.on()
print("Sensor connected, initialising...")
c.execute('''UPDATE hwstatus SET sensor = "Initialising"''')
conn.commit()
for i in range(sensor_init_time):
  print(sensor_init_time - i)
  time.sleep(1)
c.execute('''UPDATE hwstatus SET sensor = "Online"''')
conn.commit()
print("READY")



################################
# Prep CSV file fields
# alphasense keys + Timestamp
################################
fieldnames = ['Bin 0', 'Bin 1', 'Bin 2', 'Bin 3', 'Bin 4', 'Bin 5', 'Bin 6',
              'Bin 7', 'Bin 8', 'Bin 9', 'Bin 10', 'Bin 11', 'Bin 12',
              'Bin 13', 'Bin 14', 'Bin 15', 'Bin1 MToF', 'Bin3 MToF',
              'Bin5 MToF', 'Bin7 MToF', 'SFR', 'Sampling Period',
              'PM1', 'PM2.5', 'PM10', 'Checksum', 'Temperature', 'Pressure',
              'Timestamp', 'Latitude', 'Longitude', 'Altitude', 'Airspeed', 'UNIX Time']


while True:

	if runflag:

		data = alpha.histogram(number_concentration=True) # set to False for counts
		data['Timestamp'] = time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime()) # Add timestamp
		data['UNIX Time'] = time.time()
		# Add location data
		if use_autopilot_flag:
			data['Latitude'] = vehicle.location.global_frame.lat
			data['Longitude'] = vehicle.location.global_frame.lon
			data['Altitude'] = vehicle.location.global_frame.alt
			data['Airspeed'] = vehicle.airspeed
			# Add unix time
		else:
			if manual_location_flag:
				data['Latitude'] = manual_latitude
				data['Longitude'] = manual_longitude
				data['Altitude'] = manual_altitude
				data['Airspeed'] = 0
			else:
				data['Latitude'] = None
				data['Longitude'] = None
				data['Altitude'] = None
				data['Airspeed'] = None


		###################
		# Add data to CSV #
		###################
		# Check if data file exists for this day
		# If true, append data to file
		# Else, make new file and write data to it (incl header/fieldnames)
		time_str=time.strftime("%y%m%d", time.gmtime())
		if os.path.isfile('data_'+time_str+'.csv'):
		    with open('data_'+time_str+'.csv','a') as f:
		        w = csv.DictWriter(f, fieldnames, dialect='excel')
		        w.writerow(data)
		else:
		    with open('data_'+time_str+'.csv','wb') as f:
		        w = csv.DictWriter(f, fieldnames, dialect='excel')
		        w.writeheader()
		        w.writerow(data)

		###################
		# Add data to DB  #
		###################
		c.execute('''INSERT INTO opc (session_id, bin0, bin1, bin2, bin3, bin4, bin5, bin6, bin7, bin8, bin9, bin10, bin11, bin12, bin13, bin14, bin15, 
		         bin1_mtof, bin3_mtof, bin5_mtof, bin7_mtof,
		         sfr, sampling_period,
		         pM1, pm2_5, pm10,
		         checksum, temperature, pressure, timestamp,
		         latitude, longitude, altitude, airspeed, unix_Time)
		         VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 
		         ?, ?, ?, ?, 
		         ?, ?, 
		         ?, ?, ?, 
		         ?, ?, ?, ?,
		         ?, ?, ?, ?, ?)''',
		         [session_id, data['Bin 0'], data['Bin 1'], data['Bin 2'], data['Bin 3'], data['Bin 4'], data['Bin 5'], data['Bin 6'], data['Bin 7'], data['Bin 8'], data['Bin 9'], data['Bin 10'], data['Bin 11'], data['Bin 12'], data['Bin 13'], data['Bin 14'], data['Bin 15'], 
		         data['Bin1 MToF'], data['Bin3 MToF'], data['Bin5 MToF'], data['Bin7 MToF'],
		         data['SFR'], data['Sampling Period'], 
		         data['PM1'], data['PM2.5'], data['PM10'],
		         data['Checksum'], data['Temperature'], data['Pressure'], data['Timestamp'],
		         data['Latitude'], data['Longitude'], data['Altitude'], data['Airspeed'], data['UNIX Time']
		         ])
		conn.commit()

	time.sleep(sampling_period) # pause before reading data again
