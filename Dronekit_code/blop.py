from sampling_vehicle import SamplingVehicle
from dronekit import connect, Vehicle
import time
import subprocess
import usbiss
import opc

autopilot_port = "/dev/ttyS0" 
pixhawk_baud_rate = 921600
vehicle = connect(autopilot_port, baud=pixhawk_baud_rate, wait_ready=True, vehicle_class=SamplingVehicle)

sampling_period = 2 # period at which we are sampling the sensor, in seconds

# Step 1: sync system clock to GPS time from autopilot
print("Waiting for GPS time")
while vehicle.unix_time_gps.time_unix_usec == 0:
  time.sleep(1)

print("GPS time received, syncing system clock to GPS timestamp:")

try:
  subprocess.check_call(["sudo", "date", "+%s", "-s", "@"+str(vehicle.unix_time_gps.time_unix_usec/1000000.0)])
except CalledProcessError as cpe:
  print("Error setting system clock, exiting")
  print(cpe)
  sys.exit(1)

print("System clock synced succesfully to GPS time")



# Step 2: connect to the sensor 
print("Connecting to sensor")
spi = usbiss.USBISS("/dev/ttyACM0", 'spi', spi_mode = 2, freq = 500000) # Open an SPI connection
alpha = None
retry_time = 5 # time to wait between connection retries
while alpha is None:
  try:
    alpha = opc.OPCN2(spi)
  except opc.exceptions.FirmwareVersionError:
    print("Sensor is having a bad day, will try again in " + str(retry_time) + " seconds")
    time.sleep(5)

# If we reach here, we have successfully connected so turn OPC on
alpha.on()
print("Sensor connected, initialising...")
time.sleep(10)


while True:
  data = alpha.histogram(number_concentration=True)
  print(data)
  time.sleep(sampling_period)
