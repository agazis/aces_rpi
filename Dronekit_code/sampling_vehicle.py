from dronekit import Vehicle


class UnixTimeGPS(object):
    """
    UnixTimeGPS object constructor.
    """
    def __init__(self, time_boot_us=None, time_unix_usec=None):
        self.time_unix_usec = time_unix_usec

    def __str__(self):
        """
        String representation of the UnixTimeGPS object
        """
        return "UnixTimeGPS: time_unix_usec={}".format(self.time_unix_usec)



class SamplingVehicle(Vehicle):
    def __init__(self, *args):
        super(SamplingVehicle, self).__init__(*args)

        # Create a Vehicle.unix_time_gps object with initial values set to None.
        self._unix_time_gps = UnixTimeGPS()

        # Create a message listener using the decorator.
        @self.on_message('SYSTEM_TIME')
        def listener(self, name, message):
            """
            The listener is called for messages that contain the string specified in the decorator,
            passing the vehicle, message name, and the message.

            The listener writes the message to the (newly attached) ``vehicle.unix_time_gps`` object
            and notifies observers.
            """
            self._unix_time_gps.time_unix_usec=message.time_unix_usec


            # Notify all observers of new message (with new value)
            #   Note that argument `cache=False` by default so listeners
            #   are updaed with every new message
            self.notify_attribute_listeners('unix_time_gps', self._unix_time_gps)

    @property
    def unix_time_gps(self):
        return self._unix_time_gps
